package romannumeral;

import org.junit.Test;
import javax.script.ScriptException;

import static org.junit.Assert.*;

public class RomanNumeralTest {

    private RomanNumeral romanNumeral = new RomanNumeral();

    @Test
    public void testRomanNumerals() throws ScriptException {
        assertEquals("MIII", romanNumeral.romanCalculator("M + III"));
        assertEquals("III", romanNumeral.romanCalculator("X - VII"));
        assertEquals("C", romanNumeral.romanCalculator("X * X"));
        assertEquals("X", romanNumeral.romanCalculator("C / X"));
        assertEquals("XII", romanNumeral.romanCalculator("(X + XIV) / II"));
        assertEquals("VIII", romanNumeral.romanCalculator("II ^ III"));
    }
}