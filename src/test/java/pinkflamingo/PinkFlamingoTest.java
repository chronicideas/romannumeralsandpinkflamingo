package pinkflamingo;

import org.junit.Test;

import static org.junit.Assert.*;

public class PinkFlamingoTest {

    private PinkFlamingo pinkFlamingo = new PinkFlamingo();

    @Test
    public void testPinkFlamingo_1() {
        String expected = "Pink Flamingo,Flamingo,Flamingo,Flamingo,4,Flamingo,Fizz,7,Flamingo,Fizz,Buzz," +
                "11,Fizz,Flamingo,14,FizzBuzz,16,17,Fizz,19,Buzz,Flamingo,22,23,Fizz,Buzz,26,Fizz,28,29,FizzBuzz," +
                "31,32,Fizz,Flamingo,Buzz,Fizz,37,38,Fizz,Buzz," +
                "41,Fizz,43,44,FizzBuzz,46,47,Fizz,49,Buzz," +
                "Fizz,52,53,Fizz,Flamingo,56,Fizz,58,59,FizzBuzz," +
                "61,62,Fizz,64,Buzz,Fizz,67,68,Fizz,Buzz," +
                "71,Fizz,73,74,FizzBuzz,76,77,Fizz,79,Buzz," +
                "Fizz,82,83,Fizz,Buzz,86,Fizz,88,Flamingo,FizzBuzz," +
                "91,92,Fizz,94,Buzz,Fizz,97,98,Fizz,Buzz";
        String actual = pinkFlamingo.solution(0, 100);
        assertEquals(expected, actual);
    }

    @Test
    public void testPinkFlamingo_2() {
        String expected = "6761,Fizz,6763,6764,Pink Flamingo";
        String actual = pinkFlamingo.solution(6761, 6765);
        assertEquals(expected, actual);
    }

}