package romannumeral;

public class Roman {

    private final long id;
    private final String calculation;
    private final String result;

    public Roman(long id, String calculation, String result) {
        this.id = id;
        this.calculation = calculation;
        this.result = result;
    }

    public long getId() {
        return id;
    }

    public String getCalculation() {
        return calculation;
    }

    public String getResult() {
        return result;
    }
}
