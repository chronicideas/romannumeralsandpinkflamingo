package romannumeral;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class RomanNumeral {

    private final static TreeMap<Integer, String> map = new TreeMap<>();

    static {

        map.put(1000, "M");
        map.put(900, "CM");
        map.put(500, "D");
        map.put(400, "CD");
        map.put(100, "C");
        map.put(90, "XC");
        map.put(50, "L");
        map.put(40, "XL");
        map.put(10, "X");
        map.put(9, "IX");
        map.put(5, "V");
        map.put(4, "IV");
        map.put(1, "I");

    }

    private String toRoman(int number) {
        int l =  map.floorKey(number);
        if ( number == l ) {
            return map.get(number);
        }
        return map.get(l) + toRoman(number-l);
    }

    private int value(char r)
    {
        if (r == 'I')
            return 1;
        if (r == 'V')
            return 5;
        if (r == 'X')
            return 10;
        if (r == 'L')
            return 50;
        if (r == 'C')
            return 100;
        if (r == 'D')
            return 500;
        if (r == 'M')
            return 1000;
        return -1;
    }


    private int toNumber(String romanNumeral) {
        int result = 0;

        for (int i = 0; i < romanNumeral.length(); i++) {
            int s1 = value(romanNumeral.charAt(i));

            if (i + 1 < romanNumeral.length()) {
                int s2 = value(romanNumeral.charAt(i + 1));

                if (s1 >= s2) {
                    result = result + s1;
                } else {
                    result = result + s2 - s1;
                    i++;
                }
            } else {
                result = result + s1;
                i++;
            }
        }

        return result;
    }

    String romanCalculator(String calculation) throws ScriptException {
        String calculationWithoutSpaces = calculation.replaceAll(" ", "");

        String[] romanNumerals = calculationWithoutSpaces.split("[-+*/]");
        List<String> normalNumbers = new ArrayList<>();

        String regex = "([+-/*///^])";
        Matcher m = Pattern.compile(regex).matcher(calculationWithoutSpaces);
        LinkedList<String> operatorList = new LinkedList<>();

        while (m.find()) {
            operatorList.add(m.group());
        }

        for (String romanNumeral : romanNumerals) {
            if (romanNumeral.contains("^")) {
                String[] numbers = romanNumeral.split("\\^");
                normalNumbers.add(String.valueOf(Math.round(Math.pow(toNumber(numbers[0]), toNumber(numbers[1])))));
            }

            else if (romanNumeral.contains("(")) normalNumbers.add('(' + String.valueOf(toNumber(romanNumeral)));
            else if (romanNumeral.contains(")")) normalNumbers.add(String.valueOf(toNumber(romanNumeral))+ ')');
            else normalNumbers.add(String.valueOf(toNumber(romanNumeral)));
        }

        StringBuilder normalCalculation = new StringBuilder();

        for (int i=0; i<normalNumbers.size()-1; i++) {
            normalCalculation.append(normalNumbers.get(i)).append(operatorList.get(i));
        }

        normalCalculation.append(normalNumbers.get(normalNumbers.size() - 1));

        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("javascript");
        int normalNumberResult = new BigDecimal(engine.eval(normalCalculation.toString()).toString()).intValue();

        return toRoman(normalNumberResult);
    }
}
