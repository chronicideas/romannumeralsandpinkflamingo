package romannumeral;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.script.ScriptException;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class RomanController {

    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/roman")
    public Roman roman(@RequestParam(value = "calculation", defaultValue = "(I + II) + III") String calculation) throws ScriptException {
        RomanNumeral romanNumeral = new RomanNumeral();
        String result = romanNumeral.romanCalculator(calculation);

        return new Roman(counter.incrementAndGet(),
                calculation,
                result);
    }
}
