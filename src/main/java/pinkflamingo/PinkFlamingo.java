package pinkflamingo;

class PinkFlamingo {

    String solution(int startNum, int endNum) {
        StringBuilder sequence = new StringBuilder();

        for (int i = startNum; i <= endNum; i++) {
            if (isFibonacci(i)) {
                if (i%3==0 && i%5==0) {
                    sequence.append("Pink Flamingo");
                }
                else {
                    sequence.append("Flamingo");
                }
            }
            else if (i%3==0 && i%5==0) sequence.append("FizzBuzz");
            else if (i%3==0) sequence.append("Fizz");
            else if (i%5==0) sequence.append("Buzz");
            else sequence.append(i);
            sequence.append(",");
        }

        return sequence.substring(0, sequence.length()-1);
    }

    private boolean isPerfectSquare(int number) {
        int s = (int) Math.sqrt(number);
        return (s*s == number);
    }

    private boolean isFibonacci(int number) {
        return isPerfectSquare(5*number*number + 4) || isPerfectSquare(5*number*number - 4);
    }
}
