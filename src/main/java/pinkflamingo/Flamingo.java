package pinkflamingo;

public class Flamingo {

    private final long id;
    private final int startValue;
    private final int endValue;
    private final String[] result;

    Flamingo(long id, int startValue, int endValue, String[] result) {
        this.id = id;
        this.startValue = startValue;
        this.endValue = endValue;
        this.result = result;
    }

    public long getId() {
        return id;
    }

    public int getStartValue() {
        return startValue;
    }

    public int getEndValue() {
        return endValue;
    }

    public String[] getResult() {
        return result;
    }
}
