package pinkflamingo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class FlamingoController {

    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/flamingo")
    public Flamingo flamingo(@RequestParam(value = "startValue", defaultValue = "0") int startValue,
                             @RequestParam(value = "endValue", defaultValue = "100") int endValue) {
        PinkFlamingo pinkFlamingo = new PinkFlamingo();
        String[] result = pinkFlamingo.solution(startValue, endValue).split(",");
            return new Flamingo(counter.incrementAndGet(),
                startValue,
                endValue,
                result);
    }
}
